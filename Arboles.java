
package arboles;

import javax.swing.JOptionPane;
/**
 * @Bacab Ic Frida Marian-63844
 */
public class Arboles {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        
        int dato;
        String nombre;
        ArbolBinario Arbolito = new ArbolBinario();
        Arbolito.agregarNodo(1, "Demo");
        Arbolito.agregarNodo(4, "Demo");
        Arbolito.agregarNodo(7, "Demo");
        Arbolito.agregarNodo(6, "Demo");
        Arbolito.agregarNodo(3, "Demo");
        Arbolito.agregarNodo(13, "Demo");
        Arbolito.agregarNodo(14, "Demo");
        Arbolito.agregarNodo(10, "Demo");
        Arbolito.agregarNodo(9, "Demo");

        
       int opcion = 0;
       boolean salir = false;
       do{  
            
            try{
                
                 opcion = Integer.parseInt(JOptionPane.showInputDialog(null, "63844-BACAB IC FRIDA MARIAN"
                    + "\n"
                    + "\nInterfaz con menu"
                    + "\n"
                    + "\n1. InOrden"
                    + "\n2. PreOrden"
                    + "\n3. PosOrden"
                    + "\n4. salir"));
              
                
                switch(opcion){
                    
                    case 1:
                        JOptionPane.showMessageDialog(null, "Has selecionado la forma InOrden");
                         if(!Arbolito.estaVacio()){
                        Arbolito.inOrden(Arbolito.raiz);
        }
                        break;
                        
                    case 2:
                        JOptionPane.showMessageDialog(null,"Has seleccionado la forma PreOrden");
                        if(!Arbolito.estaVacio()){
                            Arbolito.preOrden(Arbolito.raiz);
         }
                        break;
                        
                    case 3:
                        JOptionPane.showMessageDialog(null,"Has Seleccionado la Forma PosOrden");
                        if(!Arbolito.estaVacio()){
                            Arbolito.posOrden(Arbolito.raiz);
         }
                        break;
                        
                    case 4:
                        JOptionPane.showMessageDialog(null,"Salir");
                     {
                   
                     }
                        salir = true;
                        break;

                    default:
                        System.out.println("Solo numeros del 1 y 4");
                }
                
            }catch(NumberFormatException e){
                 System.out.println("ERROR Ingrese un entero porfavor: " +e.getMessage());
           
            }
           }while(opcion!=4);
        }
    }
    
