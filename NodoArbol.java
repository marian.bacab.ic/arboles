/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboles;

/**
 *
 * @Bacab Ic Frida Marian-63844
 */
public class NodoArbol {
    int dato;
    String nombre;
    NodoArbol hijoizquierdo, hijoderecho;
    public NodoArbol(int d, String a){
        this.dato = d;
        this.nombre = a;
        this.hijoizquierdo = null;
        this.hijoderecho = null;
    }
        public String toString(){
            return nombre + "El dato es: "+dato;
        }
    }
